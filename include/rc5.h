#ifndef RC5_RC5_H
#define RC5_RC5_H

#define RC5_START(command) ((command & 0b11000000000000) >> 12)
#define RC5_TOGGLE(command) ((command & 0b00100000000000) >> 11)
#define RC5_ADDRESS(command) ((command & 0b00011111000000) >> 6)
#define RC5_COMMAND(command) (command & 0b00000000111111)

/**
 * Initialize the RC-5.
 */
void rc5_init();

/**
 * Reset the RC-5.
 */
void rc5_reset();

/**
 * Poll for a new command.
 * @param command_temp
 * @return
 */
uint8_t rc5_poll(uint16_t *command_temp);

#endif