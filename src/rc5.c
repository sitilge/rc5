#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../include/rc5.h"

//TICKS = PULSE_LENGTH / (1 / (CPU_FREQ / TIMER_PRESCALER))
//444us
#define SHORT_MIN 888
//1333us
#define SHORT_MAX 2666
//1334us
#define LONG_MIN 2668
//2222us
#define LONG_MAX 4444

typedef enum {
	STATE_START1,
	STATE_MID1,
	STATE_MID0,
	STATE_START0,
	STATE_ERROR,
	STATE_BEGIN,
	STATE_END
} State;

const uint8_t states[4] = {
	1,
	145,
	155,
	251
};

volatile uint16_t command;
volatile uint8_t command_new;
uint8_t counter;

State state = STATE_BEGIN;

void rc5_init()
{
	//Set output on PIND2
	DDRD &= ~(1 << PIND2);

	//Enable pull-up resistor
	PORTD |= (1 << PIND2);

	//Trigger on any edge
	MCUCR |= (1 << ISC10);

	//Enable timer1 with the prescaler of 8
	TCCR1B |= (1 << CS11);

	sei();

	rc5_reset();
}

void rc5_reset()
{
	command = 0;
	command_new = 0;
	counter = 14;
	state = STATE_BEGIN;

	//Enable INT1
	GICR |= (1 << INT1);
}

uint8_t rc5_poll(uint16_t *command_temp)
{
	if (command_new) {
		*command_temp = command;
	}

	return command_new;
}

ISR (INT1_vect)
{
	uint16_t delay = TCNT1;

//	Event numbers:
//	0 - short space
//	2 - short pulse
//	4 - long space
//	6 - long pulse

	uint8_t event = (PIND & (1 << PIND2)) ? 2 : 0;

	if (delay > LONG_MIN && delay < LONG_MAX) {
		event += 4;
	} else if (delay < SHORT_MIN || delay > SHORT_MAX) {
		rc5_reset();
	}

	if (state == STATE_BEGIN) {
		counter--;
		command |= 1 << counter;
		state = STATE_MID1;
		TCNT1 = 0;

		return;
	}

	State state_new = (states[state] >> event) & 3;

	if (state_new == state || state > STATE_START0) {
		rc5_reset();

		return;
	}

	state = state_new;

	//Emit 0 - decrement bit position counter since data is already zeroed by default
	if (state == STATE_MID0) {
		counter--;
	} else if (state == STATE_MID1) {
		//Emit 1
		counter--;
		command |= 1 << counter;
	}

	//The only valid end states are MID0 and START1
	//MID0 is ok, but if we finish in MID1 we need to wait
	//for START1 so the last edge is consumed
	if (counter == 0 && (state == STATE_START1 || state == STATE_MID0)) {
		state = STATE_END;
		command_new = 1;

		//Disable INT1
		GICR &= ~(1 << INT1);
	}

	TCNT1 = 0;
}