#define F_CPU 16000000UL

#include <avr/io.h>

#include "../include/main.h"
#include "../include/rc5.h"

int main()
{
	rc5_init();

	DDRD &= ~(1 << PIND2);

	PORTD |= (1 << PIND2);

	while (1) {
		uint16_t command;

		if (rc5_poll(&command)) {
			rc5_reset();

			uint8_t bits_start = RC5_START(command);
			uint8_t bits_toggle = RC5_TOGGLE(command);
			uint8_t bits_address = RC5_ADDRESS(command);
			uint8_t bits_command = RC5_COMMAND(command);

			//your code here
		}
	}
}