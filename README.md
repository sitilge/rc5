# RC-5

This project is a fork of [avr-rc5](https://github.com/pinkeen/avr-rc5) with some modifications to work out-of-the box with ATmega8 specific registers.

## Setup

- ATmega8 (required)
- timer1 (required)
- avr-gcc (required)
- external clock of 16MHz (optional)
- PIND2 / INT0 (optional)